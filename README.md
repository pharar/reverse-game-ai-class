# REVERSI GAME
******************************************
#### Author: Néstor Angulo de Ugarte [About Me (Pharar)](http://www.about.me/pharar "About Me (Pharar)")
#### Date: 19/06/2013


- Target:
-------------------------------------

Final project of the AI class.It is about REVERSI Game [(Wikipedia)](http://en.wikipedia.org/wiki/Reversi).
It is also called Othello and it consists in getting the more tiles in a match against another player.
This final project is made using [AIMA](http://aima.cs.berkeley.edu/) code for the search algorithms.

- Natural Division of the code
--------------------------------------

 - Board
 - Player
 - IA
 - Rules
 - AIMA