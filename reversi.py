#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# filename: reversi.py
# a simple game for the final homework of AI class in ULPGC, II'13
#
# date: 20-06-2013
# project: REVERSI GAME (http://en.wikipedia.org/wiki/Reversi).
# author: Nestor Angulo de Ugarte (http://about.me/pharar)

# Natural Division:
# - Settings
# - Board
# - Player
# - IA
# - Rules

from rules import *

if __name__ == "__main__":
    j = Reversi()
    b = j.getBoardWithValidMoves(j.state.turn)
    j.display()
    j.state = j.result(move=(6, 4))
    j.display()