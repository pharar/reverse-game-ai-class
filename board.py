#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# filename: board.py
# Definition and functions of the board of REVERSI GAME
#
# date: 20-06-2013
# project: REVERSI GAME (http://en.wikipedia.org/wiki/Reversi).
# author: Nestor Angulo de Ugarte (http://about.me/pharar)

# Local Code imports
from settings import *


# Class Board
class Board:
    """
        Represents the play board of the game
        * Attributes:
            - board: Matrix[BHEIGHT][BWIDTH]
        * Functions:
            - draw(): Prints actual status of the board
            - reset(): Resets board to initial state
            - isOnBoard(x,y): Returns True if postion x,y is on board
            - isOnCorner(x,y): Rturns True if position x,y is on a corner
            - isEmptyCell(x, y): Returns True if that position is empty
            - getBoardCopy(): Returns a Board copy from the original Board
            - setCell(x, y, value): Set value in the given position. False
                if that position is not on the board
            - getCell(x, y): Returns value in the given position. False
                if that position is not on the board
    """

    def __init__(self, h=BHEIGHT, w=BWIDTH):
        self.board = \
            [[EMPTYCELL for x in xrange(w)] for x in xrange(h)]
        self.h = h
        self.w = w

    def draw(self):
        """
            Function to Draw the actual status of the Board
            - (in) (attr) board: Matrix with the game status
        """

        line = '  \t' + '+-------'*self.w + '+'
        wall = '  \t' + '|       '*self.w + '|'

        print '     ',
        for i in range(self.w):
            print '      %i' % (i+1),
        print ' '
        print line
        for j in range(self.h):
            print wall
            print "   %i\t" % (j+1),
            for i in range(self.w):
                print '|   %s  ' % (self.board[j][i]),
            print '|'
            print wall
            print line

    def reset(self):
        """
            Function to Reset a board
            - (in) (attr) board: Matrix with the game status
        """

        # Reseting
        for j in range(self.h):
            for i in range(self.w):
                self.board[j][i] = ' '

        # Initial Status according game rules
        self.board[int(self.h/2) - 1][int(self.w/2) - 1] = P1
        self.board[int(self.h/2)][int(self.w/2) - 1] = P2
        self.board[int(self.h/2) - 1][int(self.w/2)] = P2
        self.board[int(self.h/2)][int(self.w/2)] = P1

    def isOnBoard(self, x, y):
        """
            Function to know if a position is on the board or not
            - (in) (attr) board: Matrix with the game status
            - (in) (int) x: row number
            - (in) (int) y: col number
            - (out) (bool): Is or is not on the board
            NOTE: Position is giving in 1-based way, but calculated
            as 0-based.
        """
        # Casting 1-based to 0-based
        x = x-1
        y = y-1

        return x >= 0 and x <= self.w-1 and y >= 0 and y <= self.h-1

    def isOnCorner(self, x, y):
        """
            Function to know if a position is on a corner of the the board
            or not
            - (in) (attr) board: Matrix with the game status
            - (in) (int) x: row number
            - (in) (int) y: col number
            - (out) (bool): Is or is not on a corner of the board
            NOTE: Position is giving in 1-based way, but calculated
            as 0-based.
        """
        # Casting 1-based to 0-based
        x = x-1
        y = y-1

        return (x == 0 and y == 0) or \
               (x == self.w-1 and y == 0) or \
               (x == 0 and y == self.h-1) or \
               (x == self.w-1 and y == self.h-1)

    def isEmptyCell(self, x, y):
        """
            Returns True if cell in position x,y is empty, false otherwise
            - (in) (attr) board: Matrix with the game status
            - (in) (int) x: row number
            - (in) (int) y: col number
            - (out) False -> It is a incorrect position
                    str -> Value in that position
            NOTE: x and y are 1-based
        """
        return self.board[y - 1][x - 1] == EMPTYCELL

    def getBoardCopy(self):
        """
            Returns a Board which is a copy from the original Board
            - (in) (attr) board: Matrix with the game status
            - (out) Board copy
        """
        boardcopy = Board()
        for j in range(self.h):
            for i in range(self.w):
                boardcopy.board[j][i] = self.board[j][i]

        return boardcopy

    def getCell(self, x, y):
        """
            Returns the value in that position x,y
            - (in) (attr) board: Matrix with the game status
            - (in) (int) x: row number
            - (in) (int) y: col number
            - (out) False -> It is a incorrect position
                    str -> Value in that position
            NOTE: x and y are 1-based
        """
        if self.isOnBoard(x, y):
            return self.board[y - 1][x - 1]
        else:
            return False

    def setCell(self, x, y, value):
        """
            Set the value in that position x,y
            - (in) (attr) board: Matrix with the game status
            - (in) (int) x: row number
            - (in) (int) y: col number
            - (in) (str) value: Value to be set in that position
            - (out) True -> All is correct
                    False -> It is a incorrect position
            NOTE: x and y are 1-based
        """
        if self.isOnBoard(x, y):
            self.board[y - 1][x - 1] = value
            return True
        else:
            return False
