#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# filename: rules.py
# Rules of the REVERSI GAME
#
# date: 20-06-2013
# project: REVERSI GAME (http://en.wikipedia.org/wiki/Reversi).
# author: Nestor Angulo de Ugarte (http://about.me/pharar)
#
# Using AIMA

# Imports
#import random
#import sys
# AIMA
from aimapy.games import *
# Local Code imports
from settings import *
from board import Board


class Reversi(Game):
    """
    Definitions, rules and legal moves of the REVERSI GAME
    * Attributes:
        - board[BHEIGHT][BWIDTH]
    * Functions:
        - isValidMove: Return a list of places which could be becomed by
        the player if it moves in that position x,y. Otherwise, False
        - actions(AIMA): Returns a list with valid movements for the player
        - getBoardWithValidMoves: Returns a new board with HINTS marking
        the valid moves the given player can make.
        - reset: resets the table and other values
    """

    def __init__(
        self,
        boardini=None,
        h=BHEIGHT,
        w=BWIDTH,
        turnini=P1,
        statusini=0,
        p1tilesini=2,
        p2tilesini=2,
        movesini=[]
    ):
        if not boardini or not isinstance(boardini, Board):
            boardini = Board(h, w)
            boardini.reset()

        self.h = h
        self.w = w
        self.initial = Struct(
            turn=turnini,
            status=statusini,
            board=boardini,
            p1tiles=p1tilesini,
            p2tiles=p2tilesini,
            moves=movesini,
        )
        #self.initial.moves = self.actions(self.initial)
        self.state = self.initial

    def isValidMove(self, tile, x, y, table=None):
        """
            Return a list of places which could be becomed by the player
            if it moves in that position x,y.
            Otherwise, False
            - (in) (attr) table
            - (in) (str) tile : Usually 'X' or 'O'. It could be changed in
                                settings.py
            - (in) (int) x: row number
            - (in) (int) y: col number
            - (in) (Board) table: table where the function calculates
            - (out): False -> No tiles to flip
                     list -> tiles to flip
            NOTE: self function could be threaded
        """
        if not table:
            table = self.state.board

        if not table.isEmptyCell(x, y)\
           or not table.isOnBoard(x, y):
            return False

        # Temporarily set the tile
        table.setCell(x, y, tile)

        if tile == P1:
            othertile = P2
        else:
            othertile = P1

        tiles2flip = []

        for xdirection, ydirection in [[0, 1], [1, 1], [1, 0], [1, -1],
                                      [0, -1], [-1, -1], [-1, 0], [-1, 1]]:
            auxx, auxy = x, y
            tiles2flipaux = []

            # First step
            auxx += xdirection
            auxy += ydirection

            while table.isOnBoard(auxx, auxy) and \
                    table.getCell(auxx, auxy) == othertile:
                tiles2flipaux.append((auxx, auxy))
                auxx += xdirection
                auxy += ydirection

            # If it is on the board, there are pieces to flip over.
            if table.isOnBoard(auxx, auxy) and \
               table.getCell(auxx, auxy) == tile:
                tiles2flip += list(tiles2flipaux)

        table.setCell(x, y, EMPTYCELL)
        if not len(tiles2flip):
            return False

        return tiles2flip

    def actions(self, state=None):
        """
            Returns a list of (x,y), valid moves for the given player
            on the given board.
            - (in) (attr) table
            - (in) (str) tile: player
            - (in) (Board) table: if the function is not using the table attr
            - (out): list of valid movements
        """
        if not state:
            table = self.state.board
        else:
            table = state.board

        validmoves = []

        for j in range(self.h):
            for i in range(self.w):
                if not table:
                    valid = self.isValidMove(state.turn, i, j)
                else:
                    valid = self.isValidMove(state.turn, i, j, table)
                if valid:
                    validmoves.append((i, j))
        return validmoves

    def result(self, state=None, move=(0, 0)):
        """
            Return the state that results from making a move from a
            state.
        """
        if not state:
            state = self.state

        turn = state.turn
        if move in self.actions(state):
            boardaux = state.board.getBoardCopy()
            x, y = move
            tiles2flip = self.isValidMove(turn, x, y, boardaux)
            boardaux.setCell(x, y, turn)

            #print "Tiles2flip: %s" % tiles2flip

            if tiles2flip:
                for x, y in tiles2flip:
                    boardaux.setCell(x, y, turn)
            else:
                tiles2flip = []

            if turn == P1:
                p1tilesAux = state.p1tiles + len(tiles2flip) + 1
                p2tilesAux = state.p2tiles - len(tiles2flip)
                turn2 = P2
            else:
                p1tilesAux = state.p1tiles - len(tiles2flip)
                p2tilesAux = state.p2tiles + len(tiles2flip) + 1
                turn2 = P1

            newstate = Struct(
                turn=turn2,
                status=1,
                board=boardaux,
                p1tiles=p1tilesAux,
                p2tiles=p2tilesAux,
                moves=list(state.moves)
            )
            newstate.moves.append((x, y))
            return newstate
        return state

    def getBoardWithValidMoves(self, tile, state=None):
        """
            Returns a new board with HINTS marking the valid moves
            the given player can make.
            - (in) (attr) board
            - (in) (str) tile: player
            - (out): Board with valid moves marked as HINTS
        """
        if not state:
            state = self.state

        boardaux = state.board.getBoardCopy()

        newstate = Struct(
            turn=tile,
            board=boardaux,
        )
        for x, y in self.actions(newstate):
            boardaux.setCell(x, y, HINTS)
        return boardaux

    def reset(self, state=None):
        """
            resets the table and other values
        """
        if not state:
            state = self.state
        state.board.reset()
        state.turn = P1
        state.status = 0
        state.p1tiles = 2
        state.p2tiles = 2
        state.moves = []
        self.initial = state

    def reinitiate(self, stateini=None):
        """
            Reinitiate the table and other values
        """
        if not stateini:
            stateini = self.initial
        self.state = stateini

    def to_move(self, state=None):
        "Return the player whose move it is in self state."
        if not state:
            state = self.state
        return state.turn

    def terminal_test(self, state=None):
        if not state:
            state = self.state
        if state.turn == P1:
            turn2 = P2
        else:
            turn2 = P1

        newstate = Struct(
            turn=turn2,
            board=state.board,
        )

        if not self.actions(state)\
           and not self.actions(newstate):
            #print "FIN!"
            return True
        else:
            return False

    def utility(self, state=None, player=P1):
        if not state:
            state = self.state
        if player == P1:
            utilityvalue = state.p1tiles - state.p2tiles
        else:
            utilityvalue = state.p2tiles - state.p1tiles
        #print "UTILIDAD %s = %i" % (player, utilityvalue)
        return utilityvalue

    def calculateTiles(self, tile, state=None):
        if not state:
            state = self.state
        counter = 0
        for j in range(self.h):
            for i in range(self.w):
                if state.board.getCell(i, j) == tile:
                    counter += 1
        return counter

    def display(self, state=None):
        if not state:
            state = self.state

        print "\t \t \tWelcome to REVERSI GAME"
        print "="*80
        if state.status == 0:
            statusmsg = "Ready for Start! P1 STARTS! (tiles '%s')" % P1
        elif state.status == 1:
            if state.turn == P1:
                statusmsg = "Playing (moves %i)... TURN for P1 (tiles '%s')" % (len(state.moves)/2, P1)
            else:
                statusmsg = "Playing (moves %i)... TURN for P2 (tiles '%s')" % (len(state.moves)/2, P2)
        elif state.status == 2:
            if status.p1tiles > status.p2tiles:
                statusmsg = "FINISHED!! The WINNER is P1 (tiles '%s')!!" % P1
            if status.p1tiles < status.p2tiles:
                statusmsg = "FINISHED!! The WINNER is P2 (tiles '%s')!!" % P2
            else:
                statusmsg = "FINISHED!! DRAWS P1 vs P2 (tiles '%s' and '%s')!!" % (P1, P2)
        print "\t \tStatus: %s\n" % statusmsg
        state.board.draw()

        print "\t\tPlayer 1 ('%s'): %i tiles. \t Player 2 ('%s'): %i tiles." % (P1, state.p1tiles, P2, state.p2tiles)

    def __repr__(self):
        return '<%s>' % self.__class__.__name__
