#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# filename: settings.py
# Definitions of the Game
#
# date: 20-06-2013
# project: REVERSI GAME (http://en.wikipedia.org/wiki/Reversi).
# author: Nestor Angulo de Ugarte (http://about.me/pharar)

# BOARD Global Variables
BWIDTH = 8  # Cells
BHEIGHT = 8  # Cells

# Tiles
P1 = 'X'
P2 = 'O'
EMPTYCELL = ' '
HINTS = '.'
